#!/bin/bash

CC="clang++ -target i386-unknown-linux-elf \
	--std=c++11 \
	-Wall \
	-Wextra \
	-masm=intel \
	-ffreestanding \
	-fno-pie"

mkdir -p out
mkdir -p Bootloader/out
mkdir -p Kernel/out/std

echo Assembling boot sector to binary
yasm Bootloader/src/bootsect.S -f bin -o Bootloader/out/bootsect.bin

echo Assembling kernel entry to object file
yasm Kernel/src/kernel_entry.S -f elf32 -o Kernel/out/kernel_entry.o

echo Compiling kernel sources
$CC -c Kernel/src/kernel.cpp -o Kernel/out/kernel.o
$CC -c Kernel/src/vga_console.cpp -o Kernel/out/vga_console.o
$CC -c Kernel/src/std/string.cpp -o Kernel/out/std/string.o

echo Linking kernel objects to single binary
ld.lld -melf_i386 --oformat binary -Ttext 0x1000 \
	-o Kernel/out/kernel.bin \
	Kernel/out/kernel.o \
	Kernel/out/vga_console.o \
	Kernel/out/std/string.o \
	Kernel/out/kernel_entry.o

echo Combining binaries to create image
cat Bootloader/out/bootsect.bin Kernel/out/kernel.bin > out/antos.bin
