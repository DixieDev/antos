# AntOS

An operating system I am (slowly) developing from the ground up. Latest 
developments can be found on the dev branch, while a stable release that you 
can build and run on your own machine (provided you have met the specified 
prerequisites) can be found on the master branch.

There are no big goals for this project as it is mostly just a learning 
experience. Currently it boots into 32-bit protected mode, but it would be 
nice to get into 64-bit long mode, though reading the AMD developer manuals is 
not the most enthralling way to spend my time so I'm not sure this aim will 
ever come to fruition! 

## Prerequisites

On Windows (and maybe other platforms), you must meet the following 
prerequisites in order to build and run using the provided build.sh and run.sh
scripts:
 - Have a Bash terminal installed (I just use Git Bash)
 - Have Clang installed and available on your PATH from within your Bash 
   terminal. This should include clang, clang++, and lld (specifically ld.lld)
 - Have the following LLVM target triple available: i386-unknown-linux-elf
 - Have QEmu installed, with its executables available on your PATH from within
   your Bash terminal.

You can use the Makefile if you meet the following requirements:
 - Using Linux
 - Have GCC and G++
 - Have YASM
 - Have QEmu
 - Have Make

## Building and Running
###Scripts:
With the prerequisites met, navigate to the root project directory and run
build.sh to build, then run.sh to run. There is no clean.sh to clean, for the
time being.

###Makefile:
To build the project simply run `make` in the root directory containing the 
Makefile. To run the project, make sure you have QEmu installed and run 
`make run`. You can clean up the build-related file pollution with 
`make clean`.
