#include "string.h"

void *memcpy(void *dest, const void *src, size_t len)
{
  auto *d = (char *)dest;
  auto *s = (const char *)src;
  while (len--)
    *d++ = *s++;
  return dest;
}

void *memset(void *dest, char value, size_t len)
{
  auto *d = (char *)dest;
  while (len--)
    *d++ = value;
  return dest;
}

void int_to_str(char *buf, size_t buf_len, int value)
{
  // Exit function early if buffer is too small to contain a digit
  size_t min_buf_len = value < 0 ? 3 : 2;
  if (buf_len < min_buf_len)
    return;

  // Set all characters in buffer to null char
  memset((void *)buf, '\0', buf_len);

  // If value is negative, prefix string with '-'
  if (value < 0)
  {
    buf[0] = '-';
  }

  // The number of chars that can be used for digits
  size_t max_digits = buf_len - min_buf_len + 1;

  // Determine number of digits in value
  size_t num_digits = value == 0 ? 1 : 0;
  int divided_value = value;
  while (divided_value != 0)
  {
    divided_value /= 10;
    num_digits++;
  }

  // Truncate number of digits to the maximum that will fit in the buffer
  size_t trunc_num_digits = num_digits > max_digits ? max_digits : num_digits;

  // For each digit, determine its value (in units), convert to char, then add to buffer
  unsigned int buf_index = value < 0 ? 1 : 0 + trunc_num_digits;
  int divisor = 1;
  for (unsigned int i = 0; i < trunc_num_digits; i++)
  {
    int digit = value / divisor;
    digit = digit < 0 ? -digit : digit; // Digits should be positive
    digit = digit % 10;

    char result = (char)(digit + '0');
    buf[--buf_index] = result;
    divisor *= 10;
  }
}
