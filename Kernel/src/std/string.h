#pragma once
#include <stddef.h>
#include <stdint.h>

void *memcpy(void *dest, const void *src, size_t len);
void *memset(void *dest, int value, size_t len);
void int_to_str(char *buf, size_t buf_len, int value);
