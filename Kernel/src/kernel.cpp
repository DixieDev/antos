#include "vga_console.h"
#include "std/string.h"

extern "C" void main()
{
  // Instantiate VGA_Console, a clean interface for printing to the VGA text buffer
  VGA_Console vga;
  vga.clear();

  unsigned int num = 0;
  while (true)
  {
    vga.print("##############################################\n", (num++ / 1000) % 15 + 1);
    vga.display();
  }
}
