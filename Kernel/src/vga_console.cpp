#include "vga_console.h"
#include "std/string.h"

void VGA_Console::print(const char *_str, unsigned int colour)
{
  for (unsigned int i = 0; _str[i] != '\0'; i++)
  {
    // If char is a newline character, then go to the next line and skip the rest of this iteration
    if (_str[i] == '\n')
    {
      newline();
      continue;
    }

    // If char would extend beyond the width of the screen, go to a new line before printing
    if (cursor_column + 1 >= VGA_TEXT_WIDTH)
    {
      newline();
    }

    // Write char and colour to ring buffer
    unsigned int idx = (cursor_column + cursor_row * VGA_TEXT_WIDTH) * 2;
    ring_buffer[idx] = _str[i];
    ring_buffer[idx + 1] = colour;
    cursor_column++;
  }
}

/**
 * Moves to a newline
 */
void VGA_Console::newline()
{
  // Adjust cursor position
  cursor_column = 0;
  cursor_row += 1;

  // Register whether or not scrolling mode has started
  scrolling = scrolling || cursor_row >= VGA_TEXT_HEIGHT;

  // Wrap current row within bounds
  cursor_row = cursor_row % VGA_TEXT_HEIGHT;

  // If scrolling, adjust the top row to the row after the current row (wrapped within bounds)
  if (scrolling)
  {
    top_row = (cursor_row + 1) % VGA_TEXT_HEIGHT;
  }

  // Clear the new line of any pre-existing data
  for (unsigned int col = 0; col < VGA_TEXT_WIDTH; col++)
  {
    unsigned int idx = (col + cursor_row * VGA_TEXT_WIDTH) * 2;
    ring_buffer[idx] = '\0';
    ring_buffer[idx + 1] = VGA_Text_Colour::WHITE;
  }
}

/**
 * Convenience method that prints the provided string in the same way as `print(...)`, and then 
 * moves to a newline. 
 */
void VGA_Console::println(const char *_str, unsigned int colour)
{
  print(_str, colour);
  newline();
}

/**
 * Clears the contents of the ring buffer, and resets cursor position to the top-left of the screen
 */
void VGA_Console::clear()
{
  for (unsigned int i = 0; i < VGA_TEXT_WIDTH * VGA_TEXT_HEIGHT; i++)
  {
    ring_buffer[i * 2] = '\0';
    ring_buffer[i * 2 + 1] = VGA_Text_Colour::WHITE;
  }

  cursor_column = 0;
  cursor_row = 0;
  top_row = 0;
  scrolling = false;
}

/** 
 * Writes the ring buffer into video memory, aligning so that latest text appears below everything
 * else, and lines are scrolled upwards. 
 */
void VGA_Console::display()
{
  // Understanding the code here would be vastly simplified by understanding what a ring buffer is.
  // You can find a small explanation in vga_console.h, and most likely a more exhaustive one
  // is available online.
  char *video_data = (char *)0xB8000;

  for (unsigned int row = 0; row < VGA_TEXT_HEIGHT; row++)
  {
    unsigned int buf_row = (top_row + row) % VGA_TEXT_HEIGHT;

    for (unsigned int col = 0; col < VGA_TEXT_WIDTH; col++)
    {
      unsigned int video_idx = (col + row * VGA_TEXT_WIDTH) * 2;
      unsigned int buf_idx = (col + buf_row * VGA_TEXT_WIDTH) * 2;

      video_data[video_idx] = ring_buffer[buf_idx];
      video_data[video_idx + 1] = ring_buffer[buf_idx + 1];
    }
  }
}
