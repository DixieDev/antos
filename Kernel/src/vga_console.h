#pragma once
#include <stddef.h>

const size_t VGA_TEXT_WIDTH = 80;
const size_t VGA_TEXT_HEIGHT = 25;

struct VGA_Text_Colour
{
  static const char
      BLACK = 0,
      BLUE = 1,
      GREEN = 2,
      CYAN = 3,
      RED = 4,
      PURPLE = 5,
      BROWN = 6,
      GRAY = 7,
      DARK_GRAY = 8,
      LIGHT_BLUE = 9,
      LIGHT_GREEN = 10,
      LIGHT_CYAN = 11,
      LIGHT_RED = 12,
      LIGHT_PURPLE = 13,
      YELLOW = 14,
      WHITE = 15;
};

/**
 * A convenient interface for printing scrolling text to the screen in VGA text mode.
 * The main use of this should be for logging and debugging while setting up a more modern
 * environment.
 */
struct VGA_Console
{
  /** 
   * Text data is stored in a ring buffer when methods such as `print(...)`, and `println(...)` are
   * called. A ring buffer is basically a fixed sized array that wraps around itself instead of
   * expanding. By recording the current row and the top row of the buffer, we create a sort of
   * 'view' of the buffer's data, and by sliding this view along (and wrapping it around the
   * buffer's bounds) we can easily produce a scrolling effect without reallocating every row of the
   * buffer everytime we add another line.
   */
  char ring_buffer[VGA_TEXT_WIDTH * VGA_TEXT_HEIGHT * 2];
  unsigned int cursor_column;
  unsigned int cursor_row;
  unsigned int top_row;
  bool scrolling;

  void print(const char *_str, unsigned int colour = VGA_Text_Colour::WHITE);
  void newline();
  void println(const char *_str, unsigned int colour = VGA_Text_Colour::WHITE);

  void clear();
  void display();
};
