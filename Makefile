
# Root
OS_NAME=antos

# Bootsector
BOOTSECT_BINARY=Bootloader/out/bootsect.bin

# Kernel
KERNEL_CC=g++ -m32 -ffreestanding --std=c++11 -Wall -Wextra -fno-pie -fno-exceptions -fno-rtti -masm=intel
KERNEL_SRC_DIR=Kernel/src
KERNEL_SRC_FILES=$(shell find $(KERNEL_SRC_DIR) -name '*.cpp')
KERNEL_OBJ_FILES=$(patsubst $(KERNEL_SRC_DIR)/%.cpp,Kernel/out/%.o, $(KERNEL_SRC_FILES))
KERNEL_DEP_FILES=$(patsubst $(KERNEL_SRC_DIR)/%.cpp,Kernel/dep/%.dep, $(KERNEL_SRC_FILES))
KERNEL_SRCS=$(KERNEL_SRC_DIR)/%.cpp
KERNEL_OBJS=Kernel/out/%.o
KERNEL_DEPS=Kernel/dep/%.dep

# Concat binaries to make kernel image. The order is important (bootsect must be first!)
out/$(OS_NAME).bin : $(BOOTSECT_BINARY) Kernel/out/kernel.bin
	mkdir -p $$(dirname $@)
	cat $^ > $@

# Link kernel object files
Kernel/out/kernel.bin : Kernel/out/kernel_entry.o $(KERNEL_OBJ_FILES)
	ld -melf_i386 --oformat binary -Ttext 0x1000 -o $@ $^

# Compile kernel object files
$(KERNEL_OBJS) : $(KERNEL_SRCS)
	mkdir -p $$(dirname $@)
	$(KERNEL_CC) -c $< -o $@

# Assemble kernel entry code
Kernel/out/kernel_entry.o : Kernel/src/kernel_entry.S
	mkdir -p $$(dirname $@)
	yasm "$<" -f elf32 -o $@

# Generate kernel dependency files
$(KERNEL_DEPS) : $(KERNEL_SRCS)
	mkdir -p $$(dirname $@)
	$(KERNEL_CC) $< -M -MT $(patsubst $(KERNEL_SRC_DIR)/%.cpp, Kernel/out/%.o, $<) > $@

-include $(KERNEL_DEP_FILES)

# Generate bootsector binary
$(BOOTSECT_BINARY) : Bootloader/src/bootsect.S
	mkdir -p $$(dirname $@)
	yasm "$<" -f bin -o $@

# Generate bootsector dependency files
Bootloader/dep/bootsect.dep : Bootloader/src/bootsect.S
	mkdir -p $$(dirname $@)
	echo -n "$(BOOTSECT_BINARY) : $< " > $@
	./tools/yasmdep -f "$<" -p "$$(dirname $<)/" >> "$@"

-include Bootloader/dep/bootsect.dep

clean : 
	rm -rf Bootloader/out Bootloader/dep
	rm -rf Kernel/out Kernel/dep
	rm -rf out

run : out/$(OS_NAME).bin
	qemu-system-x86_64 out/$(OS_NAME).bin
