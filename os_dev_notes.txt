OS Development Notes

===============================================================================

Boot Process:
    - BIOS loads first 512 bytes of valid boot device. Last two bytes should 
      be marked with magic number.

    - Record number stored in DL, as this is the drive the BIOS has booted from

    - Switch into protected-mode
        - Define interrupt and exception handlers
        - Define IDT
        - Define GDT
        - Load IDT pointer into IDTR
        - Load GDT pointer into GDTR
        - Set CR0.PE to 1

    - Enable A20 line...

    - Load rest of bootloader into memory

    - Set-up environment for long-mode initialisation
        - Define 64-bit mode interrupt and exception handlers
        - Define IDT with 64-bit interrupt-gate descriptors
        - Define GDT for long-mode
    
    - Disable paging by setting CR0.PG to 0

    - Enable PAE by setting CR4.PAE to 1

    - Load CR3 with physical base-address of PML4

    - Set EFER.LME to 1

    - Enable paging by setting CR0.PG to 1

    - Perform a far jump, updating CS to a 64-bit code segment

    - Load the GDT, IDT, and any LDTs (and whatever TR is meant to be) 
      registers with pointers to their respective table by using the LGDT,
      LLDT, LIDT, and LTR instructions

    - Relocate page-translation tables to desired location in physical memory

    - Reinitialise CR3 with new physical base-address of PML4

